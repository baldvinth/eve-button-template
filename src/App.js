import React, { Component } from 'react';
import axios from 'axios'
import _take from 'lodash/take'
import _uniq from 'lodash/uniq'
import './App.scss';

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      characters: [],
      errors: []
    }
  }

  componentDidMount = () => {
    document.addEventListener("keypress", event => {
      if (event.ctrlKey && (event.code === 'Enter' || event.code === 'NumpadEnter'))
        this.processCharacters()
    }, false);
  }

  getCharacterList = () => {
    const characterValues = (document.getElementById('characterNames') || {}).value

    if (characterValues === '')
      return null

    if (characterValues != null) {
      var characters = _uniq(characterValues.trim().split(/[\r\n]+/))
      return characters
    }
  }

  processCharacters = () => {
    const data = this.getCharacterList()

    if (data != null || (data || []).length > 0) {
      axios.post('https://esi.evetech.net/latest/universe/ids/?datasource=tranquility', _take(data, 9))
        .then(response => this.setState({ characters: response.data.characters }))
        .catch(err => this.setState({ errors: [...(this.state.errors || []), err.message] }))
    }
  }

  clearCharacters = () => {
    this.setState({ characters: [], errors: [] })
    document.getElementById('characterNames').value = ''
  }

  mapCharacters = () => {
    return (this.state.characters || []).map(character => {
      if (character.id != null) {
        return <div className="character" key={character.id}><img src={`https://imageserver.eveonline.com/Character/${character.id}_1024.jpg`} alt=""></img><div className="characterName">{character.name}</div></div>
      }
    })
  }

  mapErrors = () => {
    return (this.state.errors).map((err, ix) => <div key={`Error-${ix}`}>{err}</div>)
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div>
            <textarea id="characterNames" rows="9" cols="50" placeholder="Character names, one per line"></textarea>
          </div>
          <div>
            <button onClick={() => this.processCharacters()}>Submit (CTRL+Enter)</button> <button onClick={this.clearCharacters}>Clear</button>
          </div>
        </header>
        <article className="characters">
          {this.mapCharacters()}
        </article>
        <footer>
          {this.mapErrors()}
        </footer>
      </div>
    );
  }
}

export default App;
